import datetime

class Videogame:
    ultimo_numserie=0
    def __init__(self,dtfab=datetime.datetime.now(),marca=None,modelo=None,hd=None,jogos=[],garantia=None):
        self.__class__.ultimo_numserie+=1  
        self.numserie=self.__class__.ultimo_numserie
        self.marca=marca
        self.modelo=modelo
        self.dtfab=dtfab
        self.hd=hd
        self.jogos=jogos
        self.garantia=garantia

    def __str__(self):
        return f'======\nMarca: {self.marca}\nModelo: {self.modelo}\nHD: {self.hd}\nNumero de série: {self.numserie}\
\nData de Fabricação: {self.dtfab.day}/{self.dtfab.month}/{self.dtfab.year}\nJogos: {self.jogos}\nGarantia: {self.garantia}\n======\n'


if __name__=='__main__':
    vd1=Videogame() #criando sem parametro
    vd2=Videogame(datetime.datetime(2016, 6, 1)) #criando com parametro de data
    vd3=Videogame(datetime.datetime.now(),'xbox','360') #criando com parametros data, marca e modelo
    vd4=Videogame(datetime.datetime.now(),'playstation','ps4','200G',['GTAV','GoW'],'1 anos') #criando com todos os parametros
    
    print(vd1, vd2, vd3, vd4, sep='\n')